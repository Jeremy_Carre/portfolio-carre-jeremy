
function close_modal() {
    $(".modal").css("display", "none");
}

$(document).ready(function() {
    $(".check").click(function() {
        $(".modal").css("display", "block");
        $(".score").css("display", "block");
        
        
        var noAns = $(".question").filter(function() {
                      return $(this).nextUntil(".question").filter(":checked").length == 0;
        });
        if(noAns.css({ color: "red" }).length > 0) { 
            $(".score").css("display", "none");
            $('.modal_text').html(noAns.length + " question(s) n'ont pas de réponse!"); 
        }
    });
    
    $("input:radio").on("change", function() {
        $(this).prevAll(".question:first").css({ color: "" });
    });
});



function getResult() {
    var Q1 = document.getElementsByName('Q1');
    var Q2 = document.getElementsByName('Q2');
    var Q3 = document.getElementsByName('Q3');
    var Q4 = document.getElementsByName('Q4');
    var Q5 = document.getElementsByName('Q5');
    var Q6 = document.getElementsByName('Q6');
    var Q7 = document.getElementsByName('Q7');
    var Q8 = document.getElementsByName('Q8');
    var Q9 = document.getElementsByName('Q9');
    var Q10 = document.getElementsByName('Q10');

    var total = 0;

    Q1.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q2.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q3.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q4.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q5.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q6.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q7.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q8.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q9.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });
    Q10.forEach((evnt) => {
        if (evnt.checked) {
            total = total + parseInt(evnt.value);
            return;
        }
    });

    console.log("Total Marks=" + total);

    $('.score').html('Votre score est de : ' + total);
            if (total <= 16) {
                $('.modal_text').html('Un score compris entre 10 et 16 tend à indiquer une estime de soi plutôt basse.Est-ce que cela correspond à votre impression personnelle ?');
            }
            if (total >= 17 & total <= 33) {
                $('.modal_text').html('Entre 17 et 33, vous êtes dans le groupe des sujets à estime de soi moyenne. Le questionnaire ne tranche pas.\n' +
                    '\t\t\t\t\tMais peut-être pouvez-vous le faire vous-même : à quel groupe avez-vous le sentiment subjectif d’appartenir ? \n' +
                    '\t\t\t\t\tHaute ou basse estime de soi ?');
            }
            if (total >= 34) {
                $('.modal_text').html('Un score compris entre 10 et 16 tend à indiquer une estime de soi plutôt basse.<br>\n' +
                    '\t\t\t\t\tEst-ce que cela correspond à votre impression personnelle ?');
            }

}







