<?php
require 'config.php';

if (isset($_POST["action"])) {
  if ($_POST["action"] == "insert") {
    insert();
  } else if ($_POST["action"] == "edit") {
    edit();
  } else {
    delete();
  }
}


if(!empty($_GET['cmd']) )
{
    switch($_GET['cmd'])
    {
        case 'id_client_statut_pris':
 
            change_statut_to_libre();
            die();   
        ; break;
 
        case 'id_client_statut_libre': 
          
          change_statut_to_pris();
            die();   
        break;
 
        case 'id_client':
 
            if(!empty($_GET['client']) )
            {
                $result = die(json_encode(read()));
 
            }
        ; break;
  }
}



function change_statut_to_libre()
{
  global $db;
  $id_client = $_GET['id_client'];
  $query = $db->prepare(" UPDATE alarme  SET id_statut = 2
  WHERE id_client= :id_client");
  $query->bindValue(':id_client', $id_client, PDO::PARAM_INT);
  $query->execute();
  $click_libre = $query->fetch(PDO::FETCH_ASSOC);
  return ($click_libre);
}


function change_statut_to_pris()
{
  global $db;
  $id_client = $_GET['id_client'];
  $query = $db->prepare(" UPDATE alarme  SET id_statut = 1
  WHERE id_client= :id_client");
  $query->bindValue(':id_client', $id_client, PDO::PARAM_INT);
  $query->execute();
  $click_pris = $query->fetch(PDO::FETCH_ASSOC);
  return ($click_pris);
}


function read()
{
  global $db;
  $id_client = $_GET['id_client'];
  $sql = "SELECT client.id_client,client.nom_client,client.prenom_client,client.client_date_naissance,client.client_adresse,client.client_mail,
  client.client_tph, YEAR(CURDATE( )) -
  YEAR(client_date_naissance) - CASE WHEN MONTH(CURDATE( )) < MONTH(client_date_naissance) OR (MONTH(CURDATE( )) = MONTH(client_date_naissance) AND DAY(CURDATE( )) < DAY(client_date_naissance)) THEN 1 ELSE 0 END AS age FROM client
  WHERE id_client = :id_client "; 
  $query = $db->prepare($sql);
  $query->bindValue(':id_client', $id_client, PDO::PARAM_INT);
  $query->execute();
  $result = $query->fetch(PDO::FETCH_ASSOC);
  return ($result);
}


function insert()
{
  global $db;

  if (isset($_POST)) {
    if (
      isset($_POST['nom_client']) && !empty($_POST['nom_client'])
      && isset($_POST['prenom_client']) && !empty($_POST['prenom_client'])
      && isset($_POST['client_date_naissance']) && !empty($_POST['client_date_naissance'])
      && isset($_POST['client_adresse']) && !empty($_POST['client_adresse'])
      && isset($_POST['client_mail']) && !empty($_POST['client_mail'])
      && isset($_POST['client_tph']) && !empty($_POST['client_tph'])
      && isset($_POST['select_id']) && !empty($_POST['select_id'])
    ) {
      $nom_client = strip_tags($_POST['nom_client']);
      $prenom_client = strip_tags($_POST['prenom_client']);
      $client_date_naissance = strip_tags($_POST['client_date_naissance']);
      $client_adresse = strip_tags($_POST['client_adresse']);
      $client_mail = strip_tags($_POST['client_mail']);
      $client_tph = strip_tags($_POST['client_tph']);
      $id_type_alarme = strip_tags($_POST['select_id']);


      //by default, the added client is put in the free alarms.

      $sql = "INSERT INTO client (nom_client, prenom_client, client_date_naissance, client_adresse, client_mail, client_tph ) 
            VALUES ('$nom_client', '$prenom_client', '$client_date_naissance', '$client_adresse', '$client_mail', '$client_tph');
           INSERT INTO alarme (id_client, id_statut, id_type_alarme) VALUES (LAST_INSERT_ID(),'2', $id_type_alarme);
           SELECT MAX(id_client) FROM client";

      $query = $db->prepare($sql);

      $query->bindValue(':nom_client', $nom_client, PDO::PARAM_STR);
      $query->bindValue(':prenom_client', $prenom_client, PDO::PARAM_STR);
      $query->bindValue(':client_date_naissance', $client_date_naissance, PDO::PARAM_STR);
      $query->bindValue(':client_adresse', $client_adresse, PDO::PARAM_STR);
      $query->bindValue(':client_mail', $client_mail, PDO::PARAM_STR);
      $query->bindValue(':client_tph', $client_tph, PDO::PARAM_INT);
      $query->bindValue(':id_type_alarme', $id_type_alarme, PDO::PARAM_INT);

      $query->execute();
      echo "Client ajouté avec succès !";
      echo $id_type_alarme;
    }
  }
}

function edit()
{
  global $db;

  if (isset($_POST)) {
    if (
      isset($_POST['id_client']) && !empty($_POST['id_client'])
      && isset($_POST['nom_client']) && !empty($_POST['nom_client'])
      && isset($_POST['prenom_client']) && !empty($_POST['prenom_client'])
      && isset($_POST['client_date_naissance']) && !empty($_POST['client_date_naissance'])
      && isset($_POST['client_adresse']) && !empty($_POST['client_adresse'])
      && isset($_POST['client_mail']) && !empty($_POST['client_mail'])
      && isset($_POST['client_tph']) && !empty($_POST['client_tph'])
      && isset($_POST['select_id']) && !empty($_POST['select_id'])
    ) {
      $id_client = strip_tags($_POST['id_client']);
      $nom_client = strip_tags($_POST['nom_client']);
      $prenom_client = strip_tags($_POST['prenom_client']);
      $client_date_naissance = strip_tags($_POST['client_date_naissance']);
      $client_adresse = strip_tags($_POST['client_adresse']);
      $client_mail = strip_tags($_POST['client_mail']);
      $client_tph = strip_tags($_POST['client_tph']);
      $id_type_alarme = strip_tags($_POST['select_id']);


      $sql = "UPDATE client SET nom_client='$nom_client', prenom_client = '$prenom_client', client_date_naissance = '$client_date_naissance',client_adresse='$client_adresse',client_mail='$client_mail',
   client_tph='$client_tph'  WHERE id_client = '$id_client';
   UPDATE alarme SET id_type_alarme='$id_type_alarme'  WHERE id_client ='$id_client'";

      $query = $db->prepare($sql);

      $query->bindValue(':nom_client', $nom_client, PDO::PARAM_STR);
      $query->bindValue(':prenom_client', $prenom_client, PDO::PARAM_STR);
      $query->bindValue(':client_date_naissance', $client_date_naissance, PDO::PARAM_STR);
      $query->bindValue(':client_adresse', $client_adresse, PDO::PARAM_STR);
      $query->bindValue(':client_mail', $client_mail, PDO::PARAM_STR);
      $query->bindValue(':client_tph', $client_tph, PDO::PARAM_INT);
      $query->bindValue(':id_type_alarme', $id_type_alarme, PDO::PARAM_INT);

      $query->execute();

      echo "Modifié avec succès";
      echo $id_type_alarme;
    }
  }
}

function delete()
{
  global $db;

  $id_client = $_POST["action"];

  $sql = "DELETE FROM client WHERE id_client = '$id_client'";
  $query = $db->prepare($sql);
  $query->execute();
  echo "Suppression réussie";
}


$result = read();
header("Content-Type: application/json; charset=utf-8");
echo json_encode($result);
