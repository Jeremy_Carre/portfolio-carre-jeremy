// automatic refresh page every 3min
 window.setTimeout( function() {
  window.location.reload();
}, 180000); 


//get the option selected in add_client
var select_id;
function getval(sel) {
  select_id = (sel.value);
  select_id = parseInt(select_id);
  return select_id;
}

function show_add_form() {
  $("#add_section").css("display", "block");

}

function show_info_client() {
  $("#info_client").css("display", "block");
}


window.addEventListener("load", function() {
  var now = new Date();
  var utcString = now.toISOString().substring(0,19);
  var year = now.getFullYear();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  var hour = now.getHours();
  var minute = now.getMinutes();
  var second = now.getSeconds();
  var localDatetime = year + "-" +
                    (month < 10 ? "0" + month.toString() : month) + "-" +
                    (day < 10 ? "0" + day.toString() : day) + "T" +
                    (hour < 10 ? "0" + hour.toString() : hour) + ":" +
                    (minute < 10 ? "0" + minute.toString() : minute) +
                    utcString.substring(16,19);
  var datetimeField = document.getElementById("calendrier");
  datetimeField.value = localDatetime;
});


//ajax for add, edit, insert
function submitData(action) {
  $(document).ready(function () {
    var data = {
      action: action,
      id_client: $('#id_client').val(),
      nom_client: $("#nom_client").val(),
      prenom_client: $("#prenom_client").val(),
      client_date_naissance: $("#client_date_naissance").val(),
      client_adresse: $("#client_adresse").val(),
      client_mail: $("#client_mail").val(),
      client_tph: $("#client_tph").val(),
      select_id,
      
    };
    console.log(select_id);
    $.ajax({
      url: 'function.php',
      type: 'post',
      data: data,
      success: function (response) {
        alert(response);
        if (response == "Suppression réussie") {
         /*  tr.fadeOut(1000, function(){
            $(id_client).remove(); Not work, I can't explain... 
        }); */
      }
        if (response == "Client ajouté avec succès !") {
          //not found to add with ajax with my method yet. 
        }
      }
    });
  });
}



//double click function to change statut.
// I didn't find how to display the sitch statut on DOM. But all the back-end working. Becarful of double click, sometimes it doesn't work.
$(document).ready(function () {
  
  $(".id_row_client_pris").dblclick(function () {
    var datas = {
      cmd: 'id_client_statut_pris',
      id_client: $(this).attr('data-id_statut'),
    }
    $.ajax({
      url: 'function.php',
      type: 'GET',
      data: datas,
    }).done(function (click_libre) {

      //$('.list_client_libre').append(click_libre);
      
    });
  }); 

  $(".id_row_client_libre").dblclick(function () {
    var datas = {
      cmd: 'id_client_statut_libre',
      id_client: $(this).attr('data-id_statut'),
    };
    $.ajax({
      url: 'function.php',
      type: 'GET',
      data: datas,
    }).done(function (click_pris) {
      
    //$('.list_client').append(click_pris);
  
      
    });
  });
});  




//get info, push them into array

var date_client;
$(document).ready(function () {
  $(".info").click(function () {
    var datas = {
      cmd: 'id_client',
      id_client: $(this).attr('data-id'),
    };
    $.ajax({
      type: "GET",
      url: "function.php/?cmd=id_client&id_client=32",
      data: datas,
     
    }).done(function (result) {
      console.log(result.nom_type_alarme),
      $('#id').html(result.id_client)
      $('#nom').html(result.nom_client),
        $('#prenom').html(result.prenom_client),
        $('#date').html(result.client_date_naissance),
        $('#adresse').html(result.client_adresse),
        $('#mail').html(result.client_mail),
        $('#tph').html(result.client_tph),
        $('#age').html(result.age),
       
     
      $("#info_client").css("display", "block");
     
      date_client = (result.client_date_naissance);
      return date_client;
    });

    
    

    
    //mask info_client
    $('.hide_client').click(function () {
      let client_id = $(this).attr('data-id');
      $("#info_client").css("display", "none");
    });

    //get date input and number of week since start of year

    $("#calendrier").val();

    $("#calendrier").on("change", function () {

      var selected = $(this).val();
      selected = new Date(selected);

      date_final = date_client;

      date_client = new Date(date_client);

      //find the year of the selected date
      var oneJan = new Date(selected.getFullYear(), 0, 1);

      var numberOfDays = Math.floor((selected - oneJan) / (24 * 60 * 60 * 1000));

      var result_selected = Math.ceil((selected.getDay() + 1 + numberOfDays) / 7);


      //find the year of the client_date_naissance date
      var oneJan = new Date(date_client.getFullYear(), 0, 1);

      var numberOfDays = Math.floor((date_client - oneJan) / (24 * 60 * 60 * 1000));

      var result_date_client = Math.ceil((date_client.getDay() + 1 + numberOfDays) / 7);
      console.log(result_date_client)

      //apply color to td date depends of score
      score = result_date_client - result_selected;

      yellow_color = "#d1bd0e";
      if (score == -1) {
        $("#date").css("color", "red");
      }
      else if (score == 0) {
        $("#date").css("color", yellow_color);
      }
      else if (score == -2) {
        $("#date").css("color", "orange");
      }
      else {
        $("#date").css("color", "black");
      }
    });
  
});

});




/* get a row as array
var whole_row;
$(document).ready(function () {
$('.id_row_client_libre').click (function(){
  var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function() {
        whole_row=($(this).text());
        $('.list_client_pris').append.$(this);
    $(this).closest('tr').remove();
    });
});
}); */

//test with a simple switch button to display a row to another array.
 /* $(document).ready(function () {
  $('.btn_switch').click(function () {

    var row = $(this).closest('tr');
    row.clone();
    $('.list_client').append(row);
    $(this).closest('tr').remove();
  });
});  */