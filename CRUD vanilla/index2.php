<?php
require_once 'config.php';
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" type="text/css" href="css/index.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script type="text/javascript" src="script.js"> </script>


  <title>Exo test Carré jérémy</title>
</head>

<body>

  <section id="main_section">
    <h2>Index</h2>



    <div class='alarme_prise'>
      <p>Alarmes prises</p>
      <div>
        <table class="list_client" border=1>
          <tr>
            <td>#</td>
            <td>Nom</td>
            <td>Prénom</td>
            <td>Get Infos</td>
            <td>Actions</td>
          </tr>
          <?php

          $clients = $db->query('SELECT  * FROM client
        INNER JOIN alarme ON alarme.id_client = client.id_client
        WHERE alarme.id_statut=1');

          foreach ($clients as $client) :
          ?>
            <tbody>
              <tr id="tr1">
                <td class="id_row_client_pris row-id-pris pointer" data-id_statut="<?php echo $client["id_client"]; ?>"><?php echo $client["id_client"]; ?></td>
                <td class="id_row_client_pris row-name_pris pointer" data-id_statut="<?php echo $client["id_client"]; ?>"><?php echo $client["nom_client"]; ?></td>
                <td class="id_row_client_pris row-surname_pris pointer" data-id_statut="<?php echo $client["id_client"]; ?>"><?php echo $client["prenom_client"]; ?></td>
                <td><button data-id="<?php echo $client["id_client"]; ?>" type="button" class="info">Info client</button></td>
                <td>
                  <a href="edit_client.php?id_client=<?php echo $client['id_client']; ?>">Modifier</a>
                  <button type="button" onclick="submitData(<?php echo $client['id_client']; ?>);">Supprimer</button>
                  <!-- <button type="button" class="btn_switch" data-id="<?php echo $client['id_client']; ?>);">Switch statut</button> -->
                </td>
              </tr>
            </tbody>
          <?php endforeach; ?>
        </table>
      </div>
    </div>

    <div class='alarme_libre'>
      <p> Alarmes libres</p>
      <div>
        <table class="list_client" border=1>
          <tr>
            <td>#</td>
            <td>Nom</td>
            <td>Prénom</td>
            <td>Get Infos</td>
            <td>Actions</td>
          </tr>
          <?php

          $clients = $db->query('SELECT  * FROM client
        INNER JOIN alarme ON alarme.id_client = client.id_client
        WHERE alarme.id_statut=2');

          foreach ($clients as $client) :
          ?>
            <tbody>
              <tr id="tr2">
                <td class="id_row_client_libre row-id_libre  pointer" data-id_statut="<?php echo $client["id_client"]; ?>"><?php echo $client["id_client"]; ?></td>
                <td class="id_row_client_libre row-name_libre  pointer" data-id_statut="<?php echo $client["id_client"]; ?>"><?php echo $client["nom_client"]; ?></td>
                <td class="id_row_client_libre row-surname_libre  pointer" data-id_statut="<?php echo $client["id_client"]; ?>"><?php echo $client["prenom_client"]; ?></td>
                <td class="btn_info"><button data-id="<?php echo $client["id_client"]; ?>" type="button" class="info">Info client</button></td>
                <td>
                  <a href="edit_client.php?id_client=<?php echo $client['id_client']; ?>">Modifier</a>
                  <button type="button" onclick="submitData(<?php echo $client['id_client']; ?>);">Supprimer</button>
                  <!-- <button type="button" class="btn_switch" data-id="<?php echo $client['id_client']; ?>);">Switch statut</button> -->
                </td>
              </tr>
            </tbody>
          <?php endforeach; ?>
        </table>
      </div>
    </div>



    <div id="array_info">
      <table id="info_client" border=1>
        <thead>
          <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date de naissance</th>
            <th>Adresse</th>
            <th>Adresse mail</th>
            <th>Téléphone</th>
            <th>Age</th>
            

          </tr>
        </thead>

        <tbody>
          <tr id=<?php echo $client["id_client"]; ?>>
            <td id="id"></td>
            <td id="nom"></td>
            <td id="prenom"></td>
            <td id="date" name="date"></td>
            <td id="adresse"></td>
            <td id="mail"></td>
            <td id="tph"></td>
            <td id="age"></td>
           
            <td><button data-id="<?php echo $client["id_client"]; ?>" type="button" class="hide_client">Masquer client</button></td>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <br>
    <button id="btn_add" type="button" onclick="show_add_form()">Ajouter un client</button>

    <br>


    <div class="date">
      <input type="datetime-local" name="calendrier" id="calendrier" />
    </div>
  </section>

  <section id="add_section">

    <h2>Ajouter un client</h2>
    <form autocomplete="off" action="" method="post" id="add_form">
      <label for="">Nom</label>
      <input type="text" id="nom_client" value="">

      <label for="">Prénom</label>
      <input type="text" id="prenom_client" value="">

      <label for="">Date de Naissance</label>
      <input type="date" id="client_date_naissance" value="">

      <label for="">Adresse du client</label>
      <input type="text" id="client_adresse" value="">

      <label for="">Mail du client</label>
      <input type="text" id="client_mail" value="">

      <label for="">Téléphone</label>
      <input type="int" id="client_tph" value="">

      <label for="">Sélection du type d'alarme choisi par le client</label>

      <select id="id_type_alarme" name="id_type_alarme" onchange="getval(this);">
        <?php
        $id_type_alarme = $db->query('SELECT  id_type_alarme, nom_type_alarme FROM type_alarme ');
        while ($row = $id_type_alarme->fetch(PDO::FETCH_ASSOC))
          echo "<option value='" . $row['id_type_alarme'] . $row['nom_type_alarme'] . "'>" . $row['id_type_alarme'] . $row['nom_type_alarme'] . "</option>";
        ?>
      </select>

      <br>
      <button type="button" onclick="submitData('insert');">Ajouter le client</button>
    </form>
  </section>
</body>

</html>