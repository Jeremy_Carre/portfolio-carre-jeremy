<?php
try{
    // Connexion à la bdd
    $db = new PDO('mysql:host=localhost;dbname=crud_vanilla', 'crud_vanilla','crud_vanilla');
    $db->exec('SET NAMES "UTF8"');
} catch (PDOException $e){
    echo 'Erreur : '. $e->getMessage();
    die();
}
