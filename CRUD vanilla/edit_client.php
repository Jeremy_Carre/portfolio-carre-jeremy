<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/index.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script type="text/javascript" src="script.js"> </script>
  <title>Modifier un client</title>
</head>

<body>
<h3 id="h3">Modifier un client</h3>
      <form action="" method="post" id="edit_form">
        <?php
        require 'config.php';
        $id_client =  $_GET['id_client'];
        $client = $db->query("SELECT * FROM client where id_client = '$id_client'")->fetch(PDO::FETCH_ASSOC);
        ?>
        <input type="hidden" id="id_client" value="<?php echo $client['id_client']; ?>">
        <label for="nom_client">Nom</label>
        <input type="text" id="nom_client" value="<?php echo $client['nom_client']; ?>"> <br>
        <label for="prenom_client">Prénom</label>
        <input type="text" id="prenom_client" value="<?php echo $client['prenom_client']; ?>"> <br>
        <label for="client_date_naissance">Date de Naissance</label>
        <input type="date" id="client_date_naissance" value="<?php echo $client['client_date_naissance']; ?>"> <br>
        <label for="client_adresse">Adresse client</label>
        <input type="text" id="client_adresse" value="<?php echo $client['client_adresse']; ?>"> <br>
        <label for="client_mail">Adresse mail client</label>
        <input type="text" id="client_mail" value="<?php echo $client['client_mail']; ?>"> <br>
        <label for="client_tph"> téléphone client</label>
        <input type="text" id="client_tph" value="<?php echo $client['client_tph']; ?>"> <br>
        <label for="">Sélection du type d'alarme choisi par le client</label>

      <select id="id_type_alarme" name="id_type_alarme" onchange="getval(this);">
        <?php
        $id_type_alarme = $db->query('SELECT  id_type_alarme, nom_type_alarme FROM type_alarme ');
        while ($row = $id_type_alarme->fetch(PDO::FETCH_ASSOC))
        echo "<option value='" . $row['id_type_alarme'] . $row['nom_type_alarme'] . "'>" . $row['id_type_alarme'] . $row['nom_type_alarme'] . "</option>";
        ?>
      </select>
      <br>
        <button type="button" onclick="submitData('edit');">Modifier</button>
        <br>
        <a href="index2.php">Go To Index</a>
      </form>
  <br>
  

</body>

</html>