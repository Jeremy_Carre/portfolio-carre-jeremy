-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 18 août 2022 à 15:41
-- Version du serveur :  5.7.33
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `crud_vanilla`
--

-- --------------------------------------------------------

--
-- Structure de la table `alarme`
--

DROP TABLE IF EXISTS `alarme`;
CREATE TABLE IF NOT EXISTS `alarme` (
  `id_alarme` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `id_statut` int(11) NOT NULL,
  `id_type_alarme` int(11) NOT NULL,
  `ref_alarme` varchar(5) DEFAULT NULL,
  `nom_type_alarme` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_alarme`),
  KEY `alarme_client_FK` (`id_client`),
  KEY `alarme_statut0_FK` (`id_statut`),
  KEY `alarme_type_alarme1_FK` (`id_type_alarme`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `alarme`
--

INSERT INTO `alarme` (`id_alarme`, `id_client`, `id_statut`, `id_type_alarme`, `ref_alarme`, `nom_type_alarme`) VALUES
(19, 21, 1, 12, NULL, NULL),
(20, 22, 2, 12, NULL, NULL),
(22, 31, 2, 20, NULL, NULL),
(23, 32, 1, 12, NULL, NULL),
(24, 33, 2, 10, NULL, NULL),
(25, 34, 2, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `nom_client` char(50) NOT NULL,
  `prenom_client` char(50) NOT NULL,
  `client_date_naissance` date NOT NULL,
  `client_adresse` varchar(100) NOT NULL,
  `client_mail` varchar(70) NOT NULL,
  `client_tph` int(11) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `nom_client`, `prenom_client`, `client_date_naissance`, `client_adresse`, `client_mail`, `client_tph`) VALUES
(21, 'car', 'jer', '1991-06-23', '3 rue du ciel', 'jer@jh.fr', 00123456789),
(22, 'bal', 'lou', '1989-02-08', '4 rue des landes', 'lou@ah.fr', 00123456789),
(31, 'vit', 'seb', '1985-06-02', '7 rue des alpins', 'seb@aj.fr', 00123456789),
(32, 'blanc', 'raph', '2001-01-03', '8 impasse du cirque', 'raph@aj.fr', 00123456789),
(33, 'erwan', 'azar', '2000-01-01', '4 rue des geeks', 'erwan@ho.fr', 00123456789),
(34, 'lamoureux', 'quentin', '1989-08-24', '4 rue de la correze', 'quentin@hi.fr', 00123456789);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `id_statut` int(11) NOT NULL AUTO_INCREMENT,
  `nom_statut` char(10) NOT NULL,
  PRIMARY KEY (`id_statut`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`id_statut`, `nom_statut`) VALUES
(1, 'pris'),
(2, 'libre');

-- --------------------------------------------------------

--
-- Structure de la table `type_alarme`
--

DROP TABLE IF EXISTS `type_alarme`;
CREATE TABLE IF NOT EXISTS `type_alarme` (
  `id_type_alarme` int(11) NOT NULL AUTO_INCREMENT,
  `nom_type_alarme` varchar(50) NOT NULL,
  PRIMARY KEY (`id_type_alarme`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_alarme`
--

INSERT INTO `type_alarme` (`id_type_alarme`, `nom_type_alarme`) VALUES
(10, 'camera_securite'),
(12, 'alarme_intru'),
(20, 'detecteur_move');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `alarme`
--
ALTER TABLE `alarme`
  ADD CONSTRAINT `alarme_client_FK` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`) ON DELETE CASCADE,
  ADD CONSTRAINT `alarme_statut0_FK` FOREIGN KEY (`id_statut`) REFERENCES `statut` (`id_statut`) ON DELETE CASCADE,
  ADD CONSTRAINT `alarme_type_alarme1_FK` FOREIGN KEY (`id_type_alarme`) REFERENCES `type_alarme` (`id_type_alarme`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
