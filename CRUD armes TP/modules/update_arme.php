<?php
class Update_arme {
	function getAllResultats($oBdd, $GLOBALS_INI, $VARS_HTML)	{
		$spathSQL= $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_SQL"] . "update_arme.sql";
		$resultat= $oBdd->treatDatas( $spathSQL, array(
			"id_arme" => $VARS_HTML["id_arme"],
			"nom_arme" => $VARS_HTML["nom_arme"],
			"type_arme" => $VARS_HTML["type_arme"],
			"calibre_arme" => $VARS_HTML["calibre_arme"],
			"date_arme" => $VARS_HTML["date_arme"],
			"pays_arme" => $VARS_HTML["pays_arme"],
			"portee_arme" => $VARS_HTML["portee_arme"],
			"chargeur_arme" => $VARS_HTML["chargeur_arme"],
			"cadence_arme" => $VARS_HTML["cadence_arme"],
			"poids_arme" => $VARS_HTML["poids_arme"]
																	));
		return $resultat;
	}
}
?>
