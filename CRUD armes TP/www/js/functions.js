
	/**
	* detect IE
	* returns version of IE or false, if browser is not Internet Explorer or Edge
	*/
	function detectIEorSafari() {
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf('MSIE ');
		if (msie > 0) {
			// IE 10 or older
			return true;
		}
		var trident = ua.indexOf('Trident/');
		if (trident > 0) {
			// IE 11
			return true;
		}
		var edge = ua.indexOf('Edge/');
		if (edge > 0) {
			// Edge (IE 12+)
			return true;
		}
		var safari = ua.indexOf('Safari/');
		var chrome = ua.indexOf('Chrome/');
		if ((safari > 0) && (chrome == -1)) {
			// Safari
			return true;
		}
		// other browser
		return false;
	}
		
	/**
	 * Convert date aaaa-mm-jj into jj/mm/aaaa
	 */
	function convertDate(sDate)	{
		var aOfDates= sDate.split("-");
		return date_arme=aOfDates[2] + "/" + aOfDates[1] + "/" + aOfDates[0];
	}
	/**
	 * Convert date jj/mm/aaaa into aaaa-mm-jj
	 */
	function inverseDate(sDate)	{
		var aOfDates= sDate.split("/");
		return aOfDates[2] + "-" + aOfDates[1] + "-" + aOfDates[0];
	}
	/**
	 * Convert specials HTML entities HTML in character
	 */
	function htmlspecialchars_decode(str) {
		if (typeof(str) == "string") {
			str = str.replace(/&amp;/g, "&");
			str = str.replace(/&quot;/g, "\"");
			str = str.replace(/&#039;/g, "'");
			str = str.replace(/&lt;/g, "<");
			str = str.replace(/&gt;/g, ">");
		}
		return str;
	}
	
	/**
	 * public aOfarme is used to store all datas of movies
	 *
	 */
	var aOfarme= [];
	
	function loadarme()	{
		$('#divModalSaving').show();
		var datas = {
			page : "liste_arme",
			bJSON : 1
		}
		$.ajax({
			type: "POST",
			url: "route.php",
			async: true,
			data: datas,
			dataType: "json",
			cache: false,
		})
		.done(function(result) {
			console.log(result);
			var iarme= 0;
			for (var ligne in result)	{
				aOfarme[iarme]=[];
                aOfarme[iarme]["id_arme"] = result[ligne]["id_arme"];
                aOfarme[iarme]["nom_arme"] = htmlspecialchars_decode(result[ligne]["nom_arme"]);
                aOfarme[iarme]["type_arme"]= result[ligne]["type_arme"];
                aOfarme[iarme]["calibre_arme"] = result[ligne]["calibre_arme"];
                aOfarme[iarme]["date_arme"] = result[ligne]["date_arme"];
                aOfarme[iarme]["pays_arme"] =  result[ligne]["pays_arme"];
                aOfarme[iarme]["portee_arme"] = result[ligne]["portee_arme"];
                aOfarme[iarme]["chargeur_arme"] =  result[ligne]["chargeur_arme"];
                aOfarme[iarme]["cadence_arme"] =  result[ligne]["cadence_arme"];
                aOfarme[iarme]["poids_arme"] =  result[ligne]["poids_arme"];
                iarme++;
			}
			// INIT DATATABLE
			// Si je souhaite avoir par défaut autre que les 10 résultats par défaut au chargement
			// tables.page.len(10).draw();
			constructTable();
			tables = $('#table_arme').DataTable(configuration);
			$('#divModalSaving').hide();
		})
		.fail(function(err) {
			alert('error : ' + err.status);
		});
	}
	/**
	 * Parse array aOfarme and build table HTML
	 */
	 function ColorLign(type_arme) {
	var couleur = "";
	if (type_arme == "Fusil d assaut") {couleur = "Blue" };
	if (type_arme == "Pistolet") { couleur = "Orange" };
	if (type_arme == "Mitraillette") { couleur = "Vert" };
	if (type_arme == "Fusil à pompe") { couleur = "Dore" };
	if (type_arme == "Mitrailleuse") { couleur = "Rouge" };
	if (type_arme == "Fusil de précision") { couleur = "Jaune" };
	if (type_arme == "Arme spéciale") { couleur = "Gris" }

		return couleur;
	 }

     function constructTable() {
    var sHTML = "<thead>";
    sHTML += "<tr>";
	sHTML += "<td>id_arme</td>";
    sHTML += "<td>Nom</td>";
    sHTML += "<td>Type</td>";
    sHTML += "<td>Calibre</td>";
    sHTML += "<td>Date</td>";
    sHTML += "<td>Pays</td>";
    sHTML += "<td>Portee</td>";
    sHTML += "<td>Chargeur</td>";
    sHTML += "<td>Cadence (en coups par minute)</td>";
    sHTML += "<td>Poids (en kg) </td>"
    sHTML += "<td> Informations </td>";
    sHTML += "<td> Modifier </td>";
    sHTML += "<td> Supprimer </td>";
    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";

    for (var i = 0; i < aOfarme.length; i++) {

        //split= met la chaine de caractère dans un tableau et divise automatiquement
        //le ('') signifie que dès qu'il y a un espace entre les mots , c'est appliqué (fusil + d'+ assaut par ex)
        // le join  sert à concaténer les mots divisés pour n'en former qu'un seul = 1 seule class pour être compri
		var couleur = ColorLign(aOfarme[i]["type_arme"]);
        sHTML += '<tr class= "' + couleur +'">';
		sHTML += "<td>" + aOfarme[i]["id_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["nom_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["type_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["calibre_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["date_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["pays_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["portee_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["chargeur_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["cadence_arme"] + "</td>";
        sHTML += "<td>" + aOfarme[i]["poids_arme"] + "</td>";

        sHTML += "<td onClick=\" modal(" + i + ")\"> <i class=\"far fa-question-circle\"> </i> </td>";
        sHTML += "<td onClick=\"editArme(" + i + ")\"> <i class=\"fas fa-jedi\"> </i> </td>";
        sHTML += "<td onClick=\"supprimArme(" + i + ")\"> <i class=\"fas fa-skull-crossbones\"></i> </td>";
        sHTML += "</tr>";
    }
    sHTML += "</tbody>";
    $('#table_arme').html(sHTML);
}

function rebuildDatable()	{
	tables.clear();
	tables.destroy();
	constructTable();
	tables = $('#table_arme').DataTable(configuration);
}

function clearForm()	{
	$('#id_arme').val("");
	$('#nom_arme').val("");
	$('#type_arme').val("");
	$('#calibre_arme').val("");
    $('#date_arme').val("");
    $('#pays_arme').val("");
    $('#portee_arme').val("");
    $('#chargeur_arme').val("");
    $('#cadence_arme').val("");
    $('#poids_arme').val("");
    
	$('#btn_ajouter').show();
	$('#btn_modifier').hide();
	$('#btn_annuler').hide();
}

function ajoutArme()	{
	$('#divModalSaving').show();

	CheckRadio2();
	var ddatearme ;
	if (detectIEorSafari())	{
		ddatearme= inverseDate($('#date_arme').val());
	}  else  {
		ddatearme= $('#date_arme').val();
	}
	var datas = {
		page : "save_arme",
		bJSON : 1, 
		nom_arme: $('#nom_arme').val(),
		type_arme: $('#type_arme').val(),
		calibre_arme: $('#calibre_arme').val(),
		date_arme: ddatearme,
		pays_arme: $('#pays_arme').val(),
		portee_arme: CheckRadio2(),
		chargeur_arme: $('#chargeur_arme').val(),
		cadence_arme: $('#cadence_arme').val(),
		poids_arme: $('#poids_arme').val()
	}
	$.ajax({
		type: "POST",
		url: "route.php",
		async: true,
		data: datas,
		dataType: "json",
		cache: false,
	})
	.done(function(result) {
		if (result[0]["error"] != "")	{
			$('#divModalSaving').hide();
			alert("L211 lors de l'ajout de votre arme. Vous allez être déconnecté.")
		}  else  {
			var iLongueur= aOfarme.length;
			aOfarme[iLongueur]= [];
			aOfarme[iLongueur]["id_arme"]= result[0]["id_arme"];
            aOfarme[iLongueur]["nom_arme"] = $('#nom_arme').val();
        aOfarme[iLongueur]["type_arme"] = $('#type_arme').val();
        aOfarme[iLongueur]["calibre_arme"] = $('#calibre_arme').val();
        aOfarme[iLongueur]["date_arme"] = $('#date_arme').val();
        aOfarme[iLongueur]["pays_arme"] = $('#pays_arme').val();
        aOfarme[iLongueur]["portee_arme"] = CheckRadio2();
        aOfarme[iLongueur]["chargeur_arme"] = $('#chargeur_arme').val();
        aOfarme[iLongueur]["cadence_arme"] = $('#cadence_arme').val();
		aOfarme[iLongueur]["poids_arme"] = $('#poids_arme').val();
            			
			rebuildDatable();
			clearForm();
			$('#divModalSaving').hide();
		}
	})
	.fail(function(err) {
		console.log('error : ' + err.status);
		alert("L233 lors de l'ajout de votre arme. Vous allez être déconnecté.");
	});
}


function supprimArme(iIndiceSuppr)	{
	$('#divModalSaving').show();
	var datas = {
		page : "supprime_arme",
		bJSON : 1, 
		id_arme: aOfarme[iIndiceSuppr]["id_arme"]
	}
	$.ajax({
		type: "POST",
		url: "route.php",
		async: true,
		data: datas,
		dataType: "json",
		cache: false,
	})
	.done(function(result) {
		if (result[0]["error"] != "")	{
			$('#divModalSaving').hide();
			alert("Erreur lors de la suppression de votre arme. Vous allez être déconnecté.");
		}  else  {
			for (var i=iIndiceSuppr; i<(aOfarme.length-1); i++)	{
				aOfarme[i]= aOfarme[i+1];
			}
			aOfarme.length--;
			rebuildDatable();
			clearForm();
			$('#divModalSaving').hide();
		}
	})
	.fail(function(err) {
		console.log('error : ' + err.status);
		alert("Erreur lors de la suppression de votre arme. Vous allez être déconnecté.");
	});
}

var iIndiceEditionToKeep;
	function  editArme(iIndiceEdition) {
		iIndiceEditionToKeep = iIndiceEdition;
		CherchePortee();
		$('#id_arme').val(aOfarme[iIndiceEdition]["id_arme"]);
		$('#nom_arme').val(aOfarme[iIndiceEdition]["nom_arme"]);
		$('#type_arme').val(aOfarme[iIndiceEdition]["type_arme"]);
        $('#calibre_arme').val(aOfarme[iIndiceEdition]["calibre_arme"]);
		if (detectIEorSafari())	{
			$('#date_arme').val(convertDate(aOfarme[iIndiceEdition]["date_arme"]));
		}  else  {
			$('#date_arme').val(aOfarme[iIndiceEdition]["date_arme"]);
		}
		$('#pays_arme').val(aOfarme[iIndiceEdition]["pays_arme"]);
		CherchePortee(aOfarme[iIndiceEdition]["portee_arme"]);
        $('#chargeur_arme').val(aOfarme[iIndiceEdition]["chargeur_arme"]);
        $('#cadence_arme').val(aOfarme[iIndiceEdition]["cadence_arme"]);
        $('#poids_arme').val(aOfarme[iIndiceEdition]["poids_arme"]);
	}


	function majArme()	{
		$('#divModalSaving').show();
		var dDatearme, iMaj=iIndiceEditionToKeep;
		if (detectIEorSafari())	{
			dDatearme= inverseDate($('#date_arme').val());
		}  else  {
			dDatearme= $('#date_arme').val();
		}
		var datas = {
			page : "update_arme",
			bJSON : 1, 
			id_arme: aOfarme[iMaj]["id_arme"],
			nom_arme: $('#nom_arme').val(),
			type_arme: $('#type_arme').val(),
			calibre_arme: $('#calibre_arme').val(),
			date_arme: dDatearme,
			pays_arme: $('#pays_arme').val(),
			portee_arme: CheckRadio2(),
			chargeur_arme: $('#chargeur_arme').val(),
			cadence_arme: $('#cadence_arme').val(),
			poids_arme: $('#poids_arme').val(),
		}
		$.ajax({
			type: "POST",
			url: "route.php",
			async: true,
			data: datas,
			dataType: "json",
			cache: false,
		})
	 
		.done(function(result) {
			if (result[0]["error"] != "")	{
				$('#divModalSaving').hide();
				alert(" l311 Erreur lors de la modification de votre arme. Vous allez être déconnecté.");
			}  else  {
				aOfarme[iMaj]["id_arme"]= $('#id_arme').val();
				aOfarme[iMaj]["nom_arme"]= $('#nom_arme').val();
				aOfarme[iMaj]["type_arme"]= $('#type_arme').val();
				aOfarme[iMaj]["calibre_arme"]= $('#calibre_arme').val();
				aOfarme[iMaj]["date_arme"]= dDatearme;
				aOfarme[iMaj]["pays_arme"]= $('#pays_arme').val();
				aOfarme[iMaj]["portee_arme"]= CheckRadio2();
				aOfarme[iMaj]["chargeur_arme"]= $('#chargeur_arme').val();
				aOfarme[iMaj]["cadence_arme"]= $('#cadence_arme').val();
				aOfarme[iMaj]["poids_arme"]= $('#poids_arme').val();
				rebuildDatable();
				clearForm();
				$('#divModalSaving').hide();
			}
		})
		.fail(function(err) {
			console.log('error : ' + err.status);
			alert(" l367 Erreur lors de la modification de votre arme. Vous allez être déconnecté.");
		});
	}


const configuration = {
    "stateSave": false,
    "order": [[1, "asc"]],
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, 100, -1], ["Dix", "Vingt cinq", "Cinquante", "Cent","Ze total stp"]],
    "language": {
        "info": "Armes _START_ à _END_ sur _TOTAL_ sélectionnées",
        "emptyTable": "Aucune Arme",
        "lengthMenu": "_MENU_ Armes par page",
        "search": "Rechercher : ",
        "zeroRecords": "Aucun résultat de recherche",
        "paginate": {
            "previous": "Précédent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoEmpty": "Armes 0 à 0 sur 0 sélectionnée",
    },
    "columns": [
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
		{
            "orderable": true
        },
        {
            "orderable": false
        },
        {
            "orderable": false
        },
        {
            "orderable": false
        }
    ],
    'retrieve': true
};




function modal(iIndiceEdition) {
    var iIndiceEdition;
    $("#modal").css("display", "block");
    $('#Nom_mod').html(aOfarme[iIndiceEdition]["nom_arme"]);
    $('#Type_mod').html(aOfarme[iIndiceEdition]["type_arme"]);
    $('#Calibre_mod').html(aOfarme[iIndiceEdition]["calibre_arme"]);
    $('#Date_mod').html (aOfarme[iIndiceEdition]["date_arme"]);
    $('#Pays_mod').html(aOfarme[iIndiceEdition]["pays_arme"]);
    $('#Portee_mod').html(aOfarme[iIndiceEdition]["cadence_arme"]);
    $('#Chargeur_mod').html(aOfarme[iIndiceEdition]["chargeur_arme"]);
    $('#Poids_mod').html(aOfarme[iIndiceEdition]["poids_arme"]);
    $('#Cadence_mod').html(aOfarme[iIndiceEdition]["cadence_arme"]);
}

function close_mod() {
    $("#modal").css("display", "none");
}


function CherchePortee(iIndiceAfficher) {
    var porteeform = document.getElementsByName("portee_arme")

    for (let i = 0; i < porteeform.length; i++) {

        if (porteeform[i].value == iIndiceAfficher) {
            porteeform[i].checked = true;
        }
    }
}

/*afficher nombres cartouches avec range*/
function updateTextInput(val) {
    document.getElementById("Chargeur").value;

}

/*recupérer valeur bouton radios du type*/
var BoutonRadioSelect2="";
function CheckRadio2() {
    for (let i = 0; i < document.getElementsByName("portee_arme").length; i++) {
        if (document.getElementsByName("portee_arme")[i].checked) {
            BoutonRadioSelect2 = document.getElementsByName("portee_arme")[i].value;
        }
    }
	return BoutonRadioSelect2;
}

function annuler() {
    $('#nom_arme').val("");
    $("#type_arme").val("");
    $("#calibre_arme").val("");
    $("#date_arme").val("");
    $("#pays_arme").val("");
    $("#portee_arme").val("");
    $("#chargeur_arme").val("");
    $("#cadence_arme").val("");
    $("#poids_arme").val("");

	
}


var tables;
$(document).ready(function() {
	loadarme();
});

	
