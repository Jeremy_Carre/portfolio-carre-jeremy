<?php

	require "configuration.php";
	$oConfig= new Configuration();
	$GLOBALS_INI= $oConfig->getGlobalsINI();
	
	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"]  . "database.php";
	$oBdd= new Database($GLOBALS_INI["DB_HOST"], $GLOBALS_INI["DB_NAME"], $GLOBALS_INI["DB_LOGIN"], $GLOBALS_INI["DB_PSW"]);
	
	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"]  . "securite.php";
	
	$oForms= new Securite();

	
	$monPHP= $oForms->VARS_HTML["page"];
	$myClass= ucfirst($monPHP);

	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php";
	$oMain= new $myClass();
	$resultat= $oMain->getAllResultats($oBdd, $GLOBALS_INI, $oForms->VARS_HTML);
	
	unset($oBdd);
	
	if ((isset($oForms->VARS_HTML["bJSON"])) && ($oForms->VARS_HTML["bJSON"] == "1"))	{
		require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_FILES"] . $monPHP . ".html";
	}  else  {
		require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_FILES"] . "/route.html";
	}

	unset($oForms);
	
	unset($oConfig);
	
	unset($oMain);

?>
